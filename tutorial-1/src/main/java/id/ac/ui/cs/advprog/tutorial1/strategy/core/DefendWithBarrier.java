package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithBarrier implements DefenseBehavior {
        //ToDo: Complete me
        public String defend(){
            String barrier = "Defend Barrier";
            return barrier;
        }

    @Override
    public String getType() {
            String barrier = "Barrier";
            return barrier;
    }
}
