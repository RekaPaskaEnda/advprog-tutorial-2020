package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {

        public AgileAdventurer(Guild guild) {
                this.name = "Agile";
                //ToDo: Complete Me
                this.guild = guild;
        }

        //ToDo: Complete Me
        @Override
        public void update(){
                // agile bisa ambil delivery dan rumble
                if(guild.getQuestType().equals("R") || guild.getQuestType().equals("D"))
                        this.getQuests().add(guild.getQuest());

        }
}
