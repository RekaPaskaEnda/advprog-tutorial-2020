package id.ac.ui.cs.advprog.tutorial2.command.repository;

import id.ac.ui.cs.advprog.tutorial2.command.core.spell.BlankSpell;
import org.springframework.stereotype.Repository;
import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.*;
import id.ac.ui.cs.advprog.tutorial2.command.core.spell.*;

import java.util.*;

@Repository
public class ContractSeal {

    private Map<String, Spell> spells;
    private Spell latestSpell;
    private int countundo;

    public ContractSeal() {
        spells = new HashMap<>();
    }

    public void registerSpell(Spell spell) {
        spells.put(spell.spellName(), spell);
    }

    public void castSpell(String spellName) {
        // TODO: Complete Me
        countundo = 0;
        spells.get(spellName).cast();
        latestSpell = spells.get(spellName);
    }

    public void undoSpell() {
        // TODO: Complete Me
        if(countundo > 1){
            // silence
            return;
        }
        else {
            latestSpell.undo();
            countundo++;
        }



    }

    public Collection<Spell> getSpells() { return spells.values(); }
}
