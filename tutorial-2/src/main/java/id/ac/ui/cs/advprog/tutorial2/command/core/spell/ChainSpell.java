package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    ArrayList<Spell> chain;
    public ChainSpell(ArrayList<Spell> chain){
        this.chain = chain;

    }
    // TODO: Complete Me
    public void cast(){
        for(Spell spell : chain){
            spell.cast();
        }
    }
    public void undo(){
        for(int i = chain.size()-1; i>=0; i--){
            chain.get(i).undo();
        }
    }
    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
